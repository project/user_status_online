<?php

namespace Drupal\user_status_online\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user_status_online\ServiceInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\UserInterface;

/**
 * Field handler to flag ShowOnlineStatus.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("show_online_status")
 */
class ShowOnlineStatus extends FieldPluginBase {

  /**
   * Service Manager.
   *
   * @var \Drupal\user_status_online\ServiceInterface
   */
  protected $service;

  /**
   * @inheritDoc
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ServiceInterface $service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('user_status_online.service'));
  }

  /**
   * @{inheritdoc}
   */
  public function query() {
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    if ($values->_entity instanceof UserInterface) {
      $status = $this->service->getStatus($values->_entity);
      return $this->t($status);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

}
