<?php

namespace Drupal\user_status_online\StatusStrategy;

/**
 * Provides the absent strategy.
 *
 * If statement will be valid then render Absent.
 */
class AbsentStrategy extends StatusStrategy {

  /**
   * Online StatusName.
   *
   * @var string
   */
  protected $statusName = 'Absent';

  /**
   * @inheritDoc
   */
  public function isValidate(): bool {
    $last = $this->getStatus()->getUser()->getLastAccessedTime();
    $now = $this->getStatus()
      ->getRequest()
      ->getCurrentRequest()->server->get('REQUEST_TIME');
    return (($last < ($now - 400)) && $last > ($now - 800));
  }

}
