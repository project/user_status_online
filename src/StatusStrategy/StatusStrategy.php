<?php

namespace Drupal\user_status_online\StatusStrategy;

use Drupal\user_status_online\StatusInterface;

/**
 * Provides the Status Strategy Manager.
 */
abstract class StatusStrategy {

  /**
   * Strategy Status.
   *
   * @var \Drupal\user_status_online\StatusInterface
   */
  protected $status;

  /**
   * Status name string.
   *
   * @var string
   */
  protected $statusName = 'Unknown';

  /**
   * IsValidate method will be check if statement for each of strategy is valid.
   *
   * @return bool
   */
  abstract public function isValidate(): bool;

  /**
   * Retrieve statusName.
   *
   * @return string
   */
  public function getStatusName() {
    return $this->statusName;
  }

  /**
   * Get Status.
   *
   * @return \Drupal\user_status_online\StatusInterface
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * Set status.
   *
   * @param \Drupal\user_status_online\StatusInterface $status
   */
  public function setStatus(StatusInterface $status) {
    $this->status = $status;
  }

}
