<?php

namespace Drupal\user_status_online\StatusStrategy;

/**
 * Provides Online Strategy.
 *
 * If statement will be valid then render Online.
 */
class OnlineStrategy extends StatusStrategy {

  /**
   * Online StatusName.
   *
   * @var string
   */
  protected $statusName = 'Online';

  /**
   * @inheritDoc
   */
  public function isValidate(): bool {
    $last = $this->getStatus()->getUser()->getLastAccessedTime();
    $now = $this->getStatus()
      ->getRequest()
      ->getCurrentRequest()->server->get('REQUEST_TIME');
    return ($last >= ($now - 400));
  }

}
