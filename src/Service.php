<?php

namespace Drupal\user_status_online;

use Drupal\user\UserInterface;
use Drupal\user_status_online\StatusStrategy\AbsentStrategy;
use Drupal\user_status_online\StatusStrategy\OfflineStrategy;
use Drupal\user_status_online\StatusStrategy\OnlineStrategy;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Service Provide Service for Module.
 */
class Service implements ServiceInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $request;

  /**
   * Service constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The request stack.
   */
  public function __construct(RequestStack $request) {
    $this->request = $request;
  }

  /**
   * @inheritDoc
   */
  public function getStatus(UserInterface $user) {
    $statusManager = new Status($user, $this->request);
    $statusManager->addStrategy(new OnlineStrategy())
      ->addStrategy(new OfflineStrategy())
      ->addStrategy(new AbsentStrategy());

    return $statusManager->getStatus();
  }

}
