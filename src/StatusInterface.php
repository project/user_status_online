<?php

namespace Drupal\user_status_online;

use Drupal\user_status_online\StatusStrategy\StatusStrategy;

/**
 * Defines an interface for Status.
 *
 * This interface can be implemented by status that require
 * dependency injection.
 */
interface StatusInterface {

  /**
   * Add strategy to collection.
   *
   * @param \Drupal\user_status_online\StatusStrategy\StatusStrategy $strategy
   *   Status strategy object.
   *
   * @return \Drupal\user_status_online\StatusInterface
   */
  public function addStrategy(StatusStrategy $strategy);

  /**
   * Retrieve status from strategy
   *
   * @return string
   *   Status string from strategy.
   */
  public function getStatus();

  /**
   * Retrieve current user
   *
   * @return \Drupal\user\UserInterface
   *   Current User from User Account.
   */
  public function getUser();

  /**
   * Retrieve request stack
   *
   * @return \Symfony\Component\HttpFoundation\RequestStack
   *   Request from Request Stack.
   */
  public function getRequest();

}
