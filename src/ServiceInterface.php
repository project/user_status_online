<?php

namespace Drupal\user_status_online;

use Drupal\user\UserInterface;

/**
 * Interface ServiceInterface.
 */
interface ServiceInterface {

  /**
   * Retrieve status from strategy.
   *
   * @param \Drupal\user\UserInterface $user
   *   Current User from User Account.
   *
   * @return string
   *    Status string from Status Object.
   */
  public function getStatus(UserInterface $user);

}
