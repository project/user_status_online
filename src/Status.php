<?php

namespace Drupal\user_status_online;

use Drupal\user\UserInterface;
use Drupal\user_status_online\StatusStrategy\StatusStrategy;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Provide Status Container for Strategy Pattern.
 */
class Status implements StatusInterface {

  /**
   * The current user.
   *
   * @var \Drupal\user\UserInterface
   *   The current user.
   */
  protected $user;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   *   The request stack.
   */
  protected $request;


  /**
   * List all strategies.
   *
   * @var \Drupal\user_status_online\StatusStrategy\StatusStrategy[]
   *   Array of all status strategies.
   */
  private $strategies = [];

  /**
   * Constructs a new Status.
   *
   * @param \Drupal\user\UserInterface $user
   *   The current user.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The request stack.
   */
  public function __construct(UserInterface $user, RequestStack $request) {
    $this->user = $user;
    $this->request = $request;
  }

  /**
   * @inheritDoc
   */
  public function addStrategy(StatusStrategy $strategy) {
    if (!in_array($strategy, $this->strategies, TRUE)) {
      array_push($this->strategies, $strategy);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {

    foreach ($this->strategies as $strategy) {
      $strategy->setStatus($this);

      if ($strategy->isValidate()) {
        return $strategy->getStatusName();
      }
    }
    return '';

  }

  /**
   * {@inheritdoc}
   */
  public function getUser() {
    return $this->user;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequest() {
    return $this->request;
  }

}
